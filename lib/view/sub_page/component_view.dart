import 'package:flutter/material.dart';
import 'package:redmango_customer/model/data_class/component_model.dart';
import 'package:redmango_customer/model/data_class/component_section.dart';
import 'package:redmango_customer/model/data_class/product_model.dart';
import 'package:redmango_customer/utils/theme_data.dart';
import 'package:redmango_customer/view/widget/section_item_view.dart';

class ComponentsView extends StatefulWidget {
  final ProductModel productModel;
  const ComponentsView(this.productModel,{Key? key}) : super(key: key);

  @override
  _ComponentsViewState createState() => _ComponentsViewState();
}

class _ComponentsViewState extends State<ComponentsView> {
  List<ComponentSection> sections = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    widget.productModel.componentList.sort((a,b){
    if(a.properties != null && b.properties!=null){
      return a.properties!.sortOrder.compareTo(b.properties!.sortOrder);
    }else if(a.properties == null){
    return -1;
    }else {
      return 1;
    }
    });
    for (ComponentModel component in widget.productModel.componentList){
      ComponentSection? group ;
      for(ComponentSection section in sections){
        if(section.sectionValue == component.relationGroup){
          group = section;
        }
      }
      if(group != null){
        group.components.add(component);
      }else{
        group =  ComponentSection(sectionName: component.relationGroup, sectionValue: component.relationGroup, components: []);
        group.components.add(component);
          sections.add(group);
        }

    }
    for (ComponentSection section in sections){
      section.selectedItem = section.components[0];
    }
  }

  double sectionItemWidth = 100;
  double sectionItemHeight = 80;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    int axisCount = int.parse((size.width / sectionItemWidth).toString().split(".").first);
    double totalAmount = 0;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Components"),
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Flexible(
              child: ListView(
                children: [
                  Column(
                    children:sections.map((e){
                      var row = (e.components.length / axisCount).floor();
                      if((e.components.length / axisCount)%2 > .1){
                        row=row+1;
                      }

                      totalAmount+=e.selectedItem!.price;

                      return SizedBox(
                        width: size.width,
                        height: (sectionItemHeight * row)+( row * 10)+30,
                        child: Container(
                          margin: const EdgeInsets.only(left: 5,right: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Colors.black12,
                                            width: 1
                                        )
                                    )
                                ),
                                padding: const EdgeInsets.only(top: 5,bottom: 5,left: 5),
                                margin: const EdgeInsets.only(bottom: 5,left: 5,right: 5),
                                alignment: Alignment.centerLeft,
                                child: Text(e.sectionName.toUpperCase(),
                                  style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              Flexible(
                                  child: GridView(
                                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: axisCount,
                                      childAspectRatio: sectionItemWidth/sectionItemHeight,

                                    ),
                                    children: e.components.map((_item){
                                      bool selected = _item.productUUid==e.selectedItem!.productUUid;
                                      return SizedBox(
                                        width: sectionItemWidth,
                                        height: sectionItemHeight,
                                        child: Card(
                                          elevation: 2,
                                          child: Container(
                                              padding: const EdgeInsets.only(left: 5,right: 5),
                                              //color: selected?mainColor.withOpacity(.8):mainColor.withOpacity(.02),
                                              decoration: BoxDecoration(
                                                border: selected?Border.all(
                                                  color: mainColor.withOpacity(.8),
                                                  width: 3
                                                ):null
                                              ),
                                              child: InkWell(
                                                  onTap: (){
                                                    e.selectedItem = _item;
                                                    setState(() {

                                                    });
                                                  },
                                                  child: SectionItemView(_item)
                                              )
                                          ),
                                        ),
                                      );
                                    }).toList(),
                                  )
                              )
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 15,right: 15),
                    child: const TextField(
                      decoration: InputDecoration(
                          hintText: "Special instruction (If any)",
                          border: OutlineInputBorder(

                          )
                      ),
                    ),
                  ),
                ],
              )
          ),
          Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  onPressed: (){

                  },
                  child:  Text("Add to Cart ( £$totalAmount )"),
                  style:  ButtonStyle(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      shape: MaterialStateProperty.resolveWith((states) => RoundedRectangleBorder())
                  ),
                ),
              ),

            ],
          )


        ],
      ),
    );
  }
}

