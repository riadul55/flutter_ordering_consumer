import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redmango_customer/model/data_class/category_model.dart';
import 'package:redmango_customer/model/data_class/product_model.dart';
import 'package:redmango_customer/model/data_class/response_model.dart';
import 'package:redmango_customer/model/service/product_service.dart';
import 'package:redmango_customer/view/widget/homepage_item_view.dart';
import 'package:redmango_customer/viewModel/cart_view_model.dart';

class CategoryTabView extends StatefulWidget {
  final CategoryModel category;
  CategoryTabView({Key? key,required this.category}) : super(key: key);

  @override
  State<CategoryTabView> createState() => _CategoryTabViewState();
}

class _CategoryTabViewState extends State<CategoryTabView> {
  CartViewModel viewModel = Get.find();
  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: ProductService().getAllProduct(widget.category.value),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
      switch (snapshot.connectionState) {
        case ConnectionState.waiting: {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        default:
          if (snapshot.hasError) {
            return messageView("Something Wrong . Please Try Again later");
          } else{
            ApiResponse response = snapshot.data;
            List<ProductModel> products = response.data;
            return products.isNotEmpty?
                ListView(
                  children: products.map((e){
                    return HomePageProductView(e,viewModel: viewModel,);
                  }).toList()):messageView("No Item Found");
          }

      }
    },
    );
  }

  Widget messageView(String message){
    return const Center(
      child: Text("Something Wrong . Please Try Again later"),
    );
  }
}
