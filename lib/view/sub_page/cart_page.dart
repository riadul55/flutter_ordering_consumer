import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redmango_customer/view/widget/cart_item_view.dart';
import 'package:redmango_customer/viewModel/cart_view_model.dart';

class CartPage extends StatelessWidget {
   CartViewModel viewModel;
   CartPage({Key? key,required this.viewModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cart Page"),
        elevation: 0,
      ),
      body: GetBuilder<CartViewModel>(
        builder: (viewModel){
          double total = 0;
          for (var element in viewModel.cartItems) {
            total+=element.subTotal*element.quantity;
          }
          return Column(
            children: [
              Flexible(
                  child: ListView.builder(
                    itemCount: viewModel.cartItems.length,
                    itemBuilder: (ctx,i){
                      var element = viewModel.cartItems[i];
                      return CartItemView(
                        element,
                        viewModel: viewModel,
                      );
                    },
                  )
              ),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton.icon(
                        onPressed: (){

                        },
                        icon: Icon(Icons.payments_outlined),
                        label: Text(
                            "Continue To Checkout ( £ ${total.toStringAsFixed(2)} )"
                        ),
                        style: ButtonStyle(
                          shape: MaterialStateProperty.resolveWith((states) => RoundedRectangleBorder()),
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          padding: MaterialStateProperty.resolveWith((states) => EdgeInsets.only(top: 10,bottom: 10))
                        ),
                    ),
                  )
                ],
              )

            ],
          );
        },
      ),
    );
  }
}
