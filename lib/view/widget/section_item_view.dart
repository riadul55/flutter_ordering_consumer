import 'package:flutter/material.dart';
import 'package:redmango_customer/model/data_class/component_model.dart';
import 'package:redmango_customer/utils/theme_data.dart';

class SectionItemView extends StatelessWidget {
  final ComponentModel componentModel;
  const SectionItemView(this.componentModel,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
        alignment: Alignment.center,
        children: [
          RichText(
            textAlign: TextAlign.center,
              text: TextSpan(
                children: [
                  TextSpan(
                    text: componentModel.shortName,
                    style: const TextStyle(
                        fontWeight: FontWeight.w700,
                        color: Colors.black
                    ),
                  ),
                  TextSpan(
                      text: " \n( £${componentModel.price.toStringAsFixed(2)} )",
                      style: const TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.green
                      ),
                  )
                ]
              )
          ),

          // Positioned(
          //   right: 0,
          //   top: 0,
          //   child: Container(
          //       padding: EdgeInsets.only(right: 5,top: 5),
          //       child: Text("£ "+componentModel.price.toStringAsFixed(2),
          //         style: TextStyle(
          //           fontWeight: FontWeight.bold,
          //           fontSize: 15
          //         ),
          //       )
          //   ),
          // )
        ],
    );
  }
}
