import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:redmango_customer/model/data_class/cart_item_model.dart';
import 'package:redmango_customer/model/data_class/product_model.dart';
import 'package:redmango_customer/utils/theme_data.dart';
import 'package:redmango_customer/view/sub_page/component_view.dart';
import 'package:redmango_customer/viewModel/cart_view_model.dart';

class CartItemView extends StatelessWidget {
  final CartItemModel cartModel;
  CartViewModel viewModel;
  CartItemView(this.cartModel,{Key? key,required this.viewModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Container(
          height: 60,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Image.network(
                  "https://st3.depositphotos.com/1006899/14735/i/450/depositphotos_147353405-stock-photo-robot-rests-next-to-the.jpg",
                height: 60,width: 100,
              ),
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(cartModel.shortName,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold
                        ),
                      ),

                    ],
                  )
              ),
              Container(
                width: 115,
                margin: const EdgeInsets.only(right: 5,bottom: 10,top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 4),
                      child: Text("£" +(cartModel.subTotal * cartModel.quantity).toStringAsFixed(2),
                        style:  TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red[900]
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                      width: 110,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: (){
                              viewModel.deCreaseQuantity(cartModel);
                            },
                            child:  Icon(Icons.indeterminate_check_box_rounded,color: mainColor,),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5,right: 5),
                            child: Center(child: Text( cartModel.quantity.toString())),
                          ),
                          InkWell(
                            onTap: (){
                              viewModel.inCreaseQuantity(cartModel);
                            },
                            child:  Icon(Icons.add_box,color: mainColor,),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 30,
                child: InkWell(
                  onTap: (){
                    viewModel.removeFromCart(cartModel);
                  },
                  child:  Icon(Icons.delete_forever,size: 20 ,color: Colors.red[900],),
                ),
              )
            ],
          ),
        ),
    );
  }
  showComponents(context,ProductModel productModel){

    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return  ComponentsView(productModel);
        },
        fullscreenDialog: true));


    // showDialog(
    //     context: context,
    //     builder: (ctx){
    //       return AlertDialog(
    //         content: ComponentsView(productModel),
    //         scrollable: true,
    //       );
    //     }
    // );
  }
}
