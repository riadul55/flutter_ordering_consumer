import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:redmango_customer/model/data_class/cart_item_model.dart';
import 'package:redmango_customer/model/data_class/product_model.dart';
import 'package:redmango_customer/utils/theme_data.dart';
import 'package:redmango_customer/view/sub_page/component_view.dart';
import 'package:redmango_customer/viewModel/cart_view_model.dart';

class HomePageProductView extends StatelessWidget {
  final ProductModel productModel;
  CartViewModel viewModel;
  HomePageProductView(this.productModel,{Key? key,required this.viewModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("Called");
    return Card(
        child: Container(
          height: 60,
          child: InkWell(
            onTap: (){
              if(productModel.componentList.isNotEmpty){
                showComponents(context,productModel);
              }
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Image.network(
                    "https://st3.depositphotos.com/1006899/14735/i/450/depositphotos_147353405-stock-photo-robot-rests-next-to-the.jpg",
                  height: 60,width: 100,
                ),
                Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(productModel.shortName,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 3,bottom: 3),
                          child: Text(productModel.description,maxLines: 2,overflow: TextOverflow.ellipsis,style: const TextStyle(fontSize: 12),),
                        ),


                      ],
                    )
                ),
                Container(
                  margin: const EdgeInsets.only(right: 10,bottom: 10,top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text("£" +productModel.price.toStringAsFixed(2),
                          style:  TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red[900]
                          ),
                        ),
                      ),
                      Container(
                        height: 20,
                        width: 20,
                        alignment: Alignment.center,
                        child: IconButton(
                          icon: Icon(Icons.add_shopping_cart,color: mainColor,),
                          padding: EdgeInsets.zero,
                          onPressed: () {
                            if(productModel.componentList.isNotEmpty){
                                showComponents(context,productModel);
                            }else{
                              bool offer = false;
                              String category = "";
                              if(productModel.properties!=null){
                                offer = productModel.properties!.offer;
                                category = productModel.properties!.category;
                              }
                              Map _itm = CartItemModel.toJSONCartItem(itemModel: productModel,sec:[],quantity:1,subTotal: productModel.price,discountable: productModel.discountable,offerAble: offer,offered: false,category: category,);
                              viewModel.addToCart(CartItemModel.fromJSON(_itm));
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
    );
  }
  showComponents(context,ProductModel productModel){

    Navigator.of(context).push(MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return  ComponentsView(productModel);
        },
        fullscreenDialog: true));


    // showDialog(
    //     context: context,
    //     builder: (ctx){
    //       return AlertDialog(
    //         content: ComponentsView(productModel),
    //         scrollable: true,
    //       );
    //     }
    // );
  }
}
