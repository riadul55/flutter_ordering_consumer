import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redmango_customer/model/data_class/category_model.dart';
import 'package:redmango_customer/model/data_class/response_model.dart';
import 'package:redmango_customer/model/service/category_service.dart';
import 'package:redmango_customer/utils/theme_data.dart';
import 'package:redmango_customer/view/page/login_page.dart';
import 'package:redmango_customer/view/sub_page/category_tab_view.dart';
import 'package:redmango_customer/viewModel/cart_view_model.dart';

import '../sub_page/cart_page.dart';

class HomePage extends StatefulWidget {

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool loading = false;
  List<CategoryModel> categories = [];
  CartViewModel cartViewModel =   Get.put(CartViewModel());

  @override
  void initState() {
    super.initState();
    loadCategory();
  }
  loadCategory() async{
    ApiResponse response = await CategoryService().getAllCategory();
    setState(() {
      loading = true;
    });
    if(response.statusCode == 200){
      categories = response.data;
    }else{
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: const Text("Failed to load category data",style: TextStyle(color: Colors.white),),backgroundColor: Colors.red[900],)
      );
    }
    setState(() {
      loading = false;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Red Mano"),
        leading: const Icon(Icons.home),
        elevation: 0,
        actions: [
          IconButton(
              onPressed: (){},
              icon: Icon(
                Icons.notifications_active
              )
          )
        ],
      ),
      body: loading?
       Center(
        child: CircularProgressIndicator(
          color: mainColor,
        ),
      ):DefaultTabController(
        length: categories.length,
        child: Column(
          children: [
            TabBar(
                unselectedLabelColor: Colors.black.withOpacity(.7),
                labelColor: Colors.black,
                isScrollable: true,
                labelStyle: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                tabs: categories.map((e) =>
                    Tab(text: e.displayName,)
                ).toList()
            ),
            Flexible(
              child: TabBarView(
                  children: categories.map((e) =>
                      CategoryTabView(category: e)
                  ).toList()
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar:  ConvexAppBar(
        backgroundColor: mainColor,
        items: const [
           TabItem(icon: Icons.home, title: 'Home'),
           TabItem(icon: Icons.local_offer , title: 'Offer'),
           TabItem(icon: Icons.shopping_cart, title: 'Cart'),
           TabItem(icon: Icons.person, title: 'Account')
        ],
        initialActiveIndex: 0,//optional, default as 0
        onTabNotify: (i){
          if(i==2){
            Navigator.push(context, MaterialPageRoute(builder: (ctx)=>CartPage(viewModel: cartViewModel,)));
            //  showCartPage(context);
          }else if(i==3){
            Navigator.push(context, MaterialPageRoute(builder: (ctx)=>const LoginPage()));
          }
          return false;
        },

      ),
    );
  }

  showCartPage(context){

    // Navigator.of(context).push(MaterialPageRoute(
    //     builder: (BuildContext context) {
    //       return  CartPage();
    //     },
    //     fullscreenDialog: true));


    // showDialog(
    //     context: context,
    //     builder: (ctx){
    //       return AlertDialog(
    //         content: ComponentsView(productModel),
    //         scrollable: true,
    //       );
    //     }
    // );
  }

}
