import 'package:get/get.dart';
import 'package:redmango_customer/model/data_class/cart_item_model.dart';
import 'package:redmango_customer/model/service/order_helper.dart';
import 'package:redmango_customer/model/service/shared_preference.dart';

class CartViewModel extends GetxController{
  RxList<CartItemModel> cartItems = <CartItemModel>[].obs;
  RxBool val = false.obs;


  @override
  void onInit() {
    super.onInit();
    refreshLocalData();
  }

  void refreshLocalData() async{
    cartItems.clear();
    cartItems.addAll(await SharedPrefManager.getCartItems());
    update();
  }
  void syncDataToLocal() async{
    await SharedPrefManager.setCartItem(cartItems);
    refreshLocalData();
  }
  
  void addToCart(CartItemModel item){
    bool exist = alreadyExist(item,cartItems);
    if(!exist){
      item.localId = getMaxId()+1;
      if(offerExist()){
        if(item.offerAble){
          item.total = 0;
          item.type = "DYNAMIC";
          item.offered = true;
        }
      }
      cartItems.add(item);
    }
    syncDataToLocal();
  }

  void setComment(CartItemModel item){
    for (int i = 0; i<cartItems.length;i++){
      if(cartItems[i].localId == item.localId){
        cartItems[i].comment = item.comment;
      }
    }
    syncDataToLocal();
  }

  void inCreaseQuantity(CartItemModel item){
    for (int i = 0; i<cartItems.length;i++){
      if(cartItems[i].localId == item.localId){
        cartItems[i].quantity++;
      }
    }
    syncDataToLocal();
  }
  void deCreaseQuantity(CartItemModel item){
    for (int i = 0; i<cartItems.length;i++){
      if(cartItems[i].localId == item.localId){
        if(cartItems[i].quantity > 1) {
          cartItems[i].quantity--;
        }

      }
    }
    syncDataToLocal();
  }
  void removeFromCart(CartItemModel item){
    for (int i=0;i<cartItems.length;i++){
      if(cartItems[i].localId == item.localId){
        cartItems.removeWhere((element) => element.localId==item.localId);

        bool offer = offerExist();
        if(!offer){
          for (CartItemModel cart in  cartItems) {
            if(cart.offered){
              cart.offered = false;
            }
          }
        }

        break;

      }
    }
    syncDataToLocal();
  }
  void updateCartItem(CartItemModel item){
    bool exist = alreadyExist(item,cartItems);
    if(!exist){
      for (int i = 0; i<cartItems.length;i++){
        if(cartItems[i].localId == item.localId) {
          cartItems[i] =  item;
          break;
        }
      }
    }
    syncDataToLocal();
  }


  bool offerExist (){
    bool result = false;
    for(CartItemModel item in cartItems){
      if(item.category == "offer"){
        result = true;
        break;
      }
    }
    return result;
  }


  int getMaxId(){
    int max = 0;
    for (CartItemModel item in cartItems){
      if(item.localId > max) {
        max = item.localId;
      }
    }
    return max;
  }


}