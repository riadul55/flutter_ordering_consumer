import 'product_file.dart';
import 'product_properties.dart';

class ComponentModel{
   String productType,productUUid,shortName,description,relationType,relationGroup;
   double price;
   bool visible;
   ProductProperties? properties;
   List<ProductFile> files;


   ComponentModel({required this.productType, required this.productUUid, required this.shortName,
       required this.description, required this.relationType, required this.relationGroup, required this.price,
       required this.visible, required this.properties, required this.files});

   factory ComponentModel.fromJSON(Map data){
     double _price = 0;
     ProductProperties? properties;
     List<ProductFile> files = [];


     if(data["price"]!=null){
       _price = data["price"]["price"]??0;
       if(_price==0){
         _price = data["price"]["extra_price"]??0;
       }
     }
     if(data["properties"] !=null){
       properties = ProductProperties.fromJSON(data["properties"]);
     }
     for(var d in data["files"]??[]){
       files.add(ProductFile.fromJSON(d));
     }

     return ComponentModel(
       productType: data["product_type"],
       productUUid: data["product_uuid"],
       shortName: data["short_name"],
       description: data["description"],
       price:_price,
       properties: properties,
       files: files,
       relationGroup: data["relation_group"],
       relationType: data["relation_type"],
       visible: data["visible"],
     );
   }

}