// class OrderModel{
//    int db_id,shift;
//    int order_id,serverId,bookingId;
//    String paymentUid,adjustmentNote;
//    int cashId,discountPercentage;
//    String order_type,order_status,paymentMethod,paymentStatus,comments,order_channel,requester_type,discountCode;
//    double subTotal,total,totalPaid,dueAmount,tips,receive,deliveryCharge,discountAmount,refundAmount,adjustmentAmount,plasticBagCost,containerBagCost,cashPaid,cardPaid,discountableTotal;
//    String orderDate,orderDateTime,currentDeliveryTime,requestedDeliveryTime,deliveryType;
//    List<CartItem> items;
//    List<CashEntryModel> cashEntryList;
//    boolean cloudSubmitted;
//    CustomerModel customer;
//    TableBookingModel tableBookingModel;
//
//
//   public OrderModel(int order_id,String order_channel, String order_type, String order_status, String paymentMethod, double subTotal, double total,double discountableTotal,double totalPaid,double dueAmount,
//       double discountAmount,String discountCode, double tips, List<CartItem> items,String orderDateTime,String currentDeliveryTime,
//       String requestedDeliveryTime,@Nullable String deliveryType,String paymentStatus,
//       double receive,@Nullable int db_id,String orderDate,String comments,@Nullable int shift,boolean cloudSubmitted,
//       @Nullable int serverId,@Nullable CustomerModel customer,int cashId,String paymentUid,double deliveryCharge,
//       int discountPercentage,double refundAmount,double adjustmentAmount,String adjustmentNote,String requester_type,List<CashEntryModel> cashEntryList,double plasticBagCost,double containerBagCost,double cashPaid,double cardPaid,int bookingId,TableBookingModel tableBookingModel) {
//     this.order_id = order_id;
//     this.order_channel = order_channel;
//     this.order_type = order_type;
//     this.order_status = order_status;
//     this.paymentMethod = paymentMethod;
//     this.subTotal = subTotal;
//     this.total = total;
//     this.dueAmount = dueAmount;
//     this.discountAmount = discountAmount;
//     this.discountCode = discountCode;
//     this.tips = tips;
//     this.items = items;
//     this.orderDateTime = orderDateTime;
//     this.orderDate = orderDate;
//     this.currentDeliveryTime = currentDeliveryTime;
//     this.requestedDeliveryTime = requestedDeliveryTime;
//     this.deliveryType = deliveryType;
//     this.paymentStatus = paymentStatus;
//     this.receive = receive;
//     this.db_id = db_id;
//     this.comments = comments;
//     this.shift = shift;
//     this.cloudSubmitted = cloudSubmitted;
//     this.serverId = serverId;
//     this.customer = customer;
//     this.cashId = cashId;
//     this.paymentUid = paymentUid;
//     this.deliveryCharge = deliveryCharge;
//     this.discountPercentage = discountPercentage;
//     this.refundAmount = refundAmount;
//     this.adjustmentAmount =adjustmentAmount;
//     this.adjustmentNote = adjustmentNote;
//     this.requester_type = requester_type;
//     this.cashEntryList = cashEntryList;
//     this.totalPaid = totalPaid;
//     this.plasticBagCost = plasticBagCost;
//     this.containerBagCost = containerBagCost;
//     this.cardPaid = cardPaid;
//     this.cashPaid = cashPaid;
//     this.bookingId = bookingId;
//     this.tableBookingModel = tableBookingModel;
//     this.discountableTotal = discountableTotal;
//   }
//   public OrderModel() {
//
//   }
//   public static OrderModel fromJSON(JSONObject data){
//     double _subTotal = 0;
//     double _discountableTotal = 0;
//     double _total = 0;
//     double _discount = 0;
//     double _tips = 0;
//     double _deliveryCharge = 0;
//     double duoAmount = 0;
//     double totalPaid = 0;
//     double plasticBagCost = 0;
//     double containerBagCost = 0;
//     int discountPercentage = 0;
//     String discountCode = "";
//     List<CartItem> _items = new ArrayList<>();
//     List<CashEntryModel> cashEntryList = new ArrayList<>();
//     JSONArray jsonArray = data.optJSONArray("order_items");
//     JSONObject orderProperties = data.optJSONObject("order_properties");
//
//
//     if(jsonArray!= null)
//       for (int i = 0; i < jsonArray.length(); i++) {
//         CartItem _itm = CartItem.fromOnlineJSON(jsonArray.optJSONObject(i));
//         if (_itm.type.equals("DELIVERY")) {
//           _deliveryCharge = _itm.total;
//         } else if (_itm.type.equals("DISCOUNT")) {
//           discountCode = _itm.description;
//         } else {
//           _subTotal += (_itm.total);
//           if (_itm.uuid.equals("plastic-bag")) {
//             plasticBagCost = _itm.quantity * _itm.price;
//           } else if (_itm.uuid.equals("container")) {
//             containerBagCost = _itm.quantity * _itm.price;
//           } else {
//
//             if(_itm.type.equals("DYNAMIC")){
//               _itm.offered = true;
//               if(orderProperties != null){
//                 _itm.printOrder = orderProperties.optInt("print_"+_itm.uuid);
//               }
//
//             }
//             _items.add(_itm);
//             if (_itm.discountable) {
//               _discountableTotal += _itm.total;
//             }
//           }
//
//         }
//       }
//     _total = _subTotal;
//     _total += _deliveryCharge;
//     if (_total == 0) {
//       _total = data.optDouble("discountable_amount", 0) + data.optDouble("delivery_charges", 0);
//     }
//     String comments = "";
//     if (orderProperties != null) {
//       comments = orderProperties.optString("comment","");
//     }
//     CustomerModel customerModel = null;
//     if (data.opt("requester") != null) {
//       JSONObject info = data.optJSONObject("requester");
//       customerModel = CustomerModel.fromJSON(info);
//       customerModel.addresses = new ArrayList<>();
//       if (data.optJSONObject("shipping_address") != null) {
//         customerModel.addresses.add(CustomerAddress.fromJSON(data.optJSONObject("shipping_address")));
//       }
//     } else {
//       Log.d("mmmmmm", "fromJSON: requester null found");
//     }
//     _discount = data.optDouble("discounted_amount", 0.0);
//     String requestedDeliveryTime = data.optString("requested_delivery_timestamp");
//     if (requestedDeliveryTime.equals("")) {
//       try {
//         data.put("current_delivery_timestamp", requestedDeliveryTime);
//       } catch (JSONException e) {
//     Log.d("mmmmmm", "fromJSON: " + e.getMessage());
//     e.printStackTrace();
//     }
//     }
//     String orderDate = DateTimeHelper.formatServerDateTime(data.optString("order_date", DateTimeHelper.getTime()));
//
//     if (data.optJSONArray("cash_entries") != null) {
//     JSONArray _list = data.optJSONArray("cash_entries");
//     for (int i = 0; i < _list.length(); i++) {
//     CashEntryModel _itm = CashEntryModel.fromJSON(_list.optJSONObject(i));
//     totalPaid += _itm.amount;
//     cashEntryList.add(_itm);
//     }
//     }
//
//     return new OrderModel(
//     -1,
//     data.optString("order_channel"),
//     data.optString("order_type"),
//     data.optString("order_status"),
//     data.optString("payment_type"), _subTotal, _total, _discountableTotal, totalPaid, duoAmount, _discount, discountCode,
//     _tips,
//     _items,
//     orderDate,
//     DateTimeHelper.formatServerDateTime(data.optString("current_delivery_timestamp")),
//     DateTimeHelper.formatServerDateTime(data.optString("requested_delivery_timestamp")),
//     "",
//     "PAID",
//     _total,
//     -1,
//     orderDate.substring(0, 10),
//     comments,
//     -1,
//     false,
//     data.optInt("order_id"),
//     customerModel,
//     data.optInt("cash_id"),
//     data.optString("payment_uuid"),
//     _deliveryCharge,
//     discountPercentage, 0, 0, "",
//     data.optString("order_requester_type"),
//     cashEntryList,
//     plasticBagCost, containerBagCost, 0, 0, data.optInt("booking_id", 0), null
//     );
//   }
//
// }
//
//
