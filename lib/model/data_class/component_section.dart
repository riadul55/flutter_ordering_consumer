import 'package:redmango_customer/model/data_class/component_model.dart';

class ComponentSection{
  String sectionName,sectionValue;
  List<ComponentModel> components;
  ComponentModel? selectedItem;


  ComponentSection({required this.sectionName, required this.sectionValue, required this.components, this.selectedItem});

  factory ComponentSection.fromJSON(Map data){
    ComponentModel? selected;
    List<ComponentModel> componentList = [];
    for(var d in data["components"]??[]){
      componentList.add(ComponentModel.fromJSON(d));

    }

    if(data["selected"] != null){
      selected = ComponentModel.fromJSON(data["selected"]);
    }
    return ComponentSection(
        sectionName: data["sectionName"],
        sectionValue: data["sectionValue"],
        components: componentList,
        selectedItem: selected
    );

  }
}