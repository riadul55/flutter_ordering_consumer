class CategoryModel{
  String keyName;
  String value;
  String displayName;
  String description;
  int sortOrder;
  String fileName;
  String contextType;

  CategoryModel({required this.keyName,required this.value,required this.displayName,required this.description,required this.sortOrder,required this.fileName,required this.contextType});
  factory CategoryModel.fromJSON(data){
    return CategoryModel(
      keyName: data["key_name"],
      value: data["value"],
      displayName: data["display_name"],
      description: data["description"]??"",
      sortOrder: data["sort_order"],
      fileName: data["filename"],
      contextType: data["content_type"],
    );
  }
}