
import 'component_model.dart';
import 'component_section.dart';
import 'product_model.dart';

class CartItemModel{
  String uuid, description, type, shortName;
  List<ComponentSection> componentSections;
  List<CartItemModel> extra;
  double price,offerPrice;
  double subTotal, total;
  int quantity, printOrder,localId;
  String comment,category;
  bool discountable,offerAble,offered;

  CartItemModel({
      required this.localId,
      required this.uuid,
      required this.description,
      required this.type,
      required this.shortName,
      required this.componentSections,
      required this.extra,
      required this.price,
      required this.offerPrice,
      required this.subTotal,
      required this.total,
      required this.quantity,
      required this.printOrder,
      required this.comment,
      required this.category,
      required this.discountable,
      required this.offerAble,
      required this.offered});

  factory CartItemModel.fromJSON(Map data) {
    List<ComponentSection> componentSections = [];
    List<CartItemModel> extra =[];

    for(var d in data["components"]??[]){
      componentSections.add(ComponentSection.fromJSON(d));
    }
    for(var d in data["extra"]??[]){
      extra.add(CartItemModel.fromJSON(d));
    }
    return CartItemModel(
      localId: data["localId"]??0,
      uuid: data["product_uuid"],
      description: data["description"]??"",
      shortName: data["short_name"]??"N/A",
      componentSections: componentSections,
      price: data["price"]??0,
      offerPrice: data["offerPrice"]??0,
      subTotal: data["subTotal"]??0,
      total: data["Total"]??0,
      quantity: data["quantity"]??1,
      comment: data["comment"]??"",
      type: data["product_type"]??"",
      printOrder: data["print_order"]??0,
      discountable: data["discountable"]??false,
      offerAble: data["offerAble"]??false,
      offered: data["offered"]??false,
      category: data["category"]??"",
      extra: extra
    );
  }

  static Map<String,dynamic> toJSON({required CartItemModel itemModel}){
    return {
      "product_uuid":itemModel.uuid,
      "short_name":itemModel.shortName,
      "description":itemModel.description,
      "price":itemModel.price,
      "offerPrice":itemModel.offerPrice,
      "subTotal":itemModel.subTotal,
      "Total":itemModel.total,
      "quantity":itemModel.quantity,
      "components":itemModel.componentSections,
      "product_type":itemModel.type,
      "print_order":itemModel.printOrder,
      "localId":itemModel.localId,
      "discountable": itemModel.discountable,
      "offerAble":itemModel.discountable,
      "offered":itemModel.offered,
      "category":itemModel.category,
      "extra":itemModel.extra,
    };
  }

  static Map<String,dynamic> toJSONCartItem({required ProductModel itemModel,required List<ComponentSection> sec,required int quantity,required double subTotal,required bool discountable,required bool offerAble,required bool offered,required String category}) {
    List<Map<String,dynamic>> _sectionList =[];

    int printOrder = 0;
    List<ComponentSection> _sections = [];

    List<ComponentModel> componentArrayList = itemModel.componentList;

      for (ComponentModel component in componentArrayList) {
        ComponentSection? group;
        for (ComponentSection section in _sections) {
          if (section.sectionValue == component.relationGroup) {
            group = section;
          }
        }
        if (group != null) {
          group.components.add(component);
        } else {
          group = ComponentSection( sectionValue: component.relationGroup,sectionName: component.relationGroup, components: [], );
          group.components.add(component);
          _sections.add(group);
        }

      }
      for (ComponentSection section in _sections) {
        section.selectedItem = section.components.first;
      }

    if(itemModel.properties != null){
      printOrder = itemModel.properties!.printOrder;
    }

    return {
      "product_uuid":itemModel.productUUid,
      "short_name":itemModel.shortName,
      "description":itemModel.description,
      "price":itemModel.price,
      "offerPrice":itemModel.offerPrice,
      "subTotal":subTotal,
      "Total":subTotal,
      "quantity":quantity,
      "components":_sectionList,
      "product_type":itemModel.productType,
      "print_order":printOrder,
      "localId":0,
      "discountable": discountable,
      "offerAble":offerAble,
      "offered":offered,
      "category":category,
      "extra":[],
    };
  }

}