class ApiResponse{
  int? statusCode;
  String? message;
  dynamic data;
  ApiResponse({this.data,this.message,this.statusCode});

}