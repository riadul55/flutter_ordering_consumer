import 'package:redmango_customer/model/data_class/component_model.dart';
import 'package:redmango_customer/model/data_class/product_file.dart';
import 'package:redmango_customer/model/data_class/product_properties.dart';

class ProductModel{
   String productType,productUUid,shortName,description;
   double price,offerPrice;
   bool visible;
   bool discountable;
   ProductProperties? properties;
   List<ProductFile> files;
   List<ComponentModel> componentList;


   ProductModel({required this.productType, required this.productUUid, required this.shortName,
       required this.description, required this.price, required this.offerPrice, required this.visible, required this.discountable,this.properties, required this.files, required this.componentList});

   factory ProductModel.fromJSON(Map data){
     double _price = 0;
     double offerPrice  = 0;
     ProductProperties? properties;
     List<ProductFile> files = [];
     List<ComponentModel> components  =[];

     if(data["price"]!=null){
       _price = data["price"]["price"]??0;
       if(_price==0){
         _price = data["price"]["extra_price"]??0;
       }
     }

     if(data["properties"] !=null){
         properties = ProductProperties.fromJSON(data["properties"]);
         offerPrice = properties.offerPrice;
     }

     for(var d in data["files"]??[]){
       files.add(ProductFile.fromJSON(d));
     }

     if(data["components"] != null){
       for(var d in data["components"]??[]){
         components.add(ComponentModel.fromJSON(d));
       }
     }else if((data["product_type"]??"ITEM") == "BUNDLE"){
       for(var d in data["items"]??[]){
         components.add(ComponentModel.fromJSON(d));
       }
     }

     return ProductModel(
       productType: data["product_type"],
       productUUid: data["product_uuid"],
       shortName: data["short_name"],
       description: data["description"],
       price:_price,
       properties: properties,
       files: files,
       visible: data["visible"]??false,
       discountable: data["discountable"]??false,
       offerPrice: offerPrice,
       componentList: components,
     );
   }
}

