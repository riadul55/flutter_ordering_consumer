class ProductProperties{
   bool D,V,G,N,VE,offer;
   String category,spiced,availableOn;
   int sortOrder,printOrder;
   double offerPrice;


   ProductProperties({required this.D, required this.V, required this.G, required this.N, required this.VE, required this.offer,
       required this.category, required this.spiced, required this.availableOn, required this.sortOrder,
       required this.printOrder, required this.offerPrice});

   factory ProductProperties.fromJSON(data){
     return ProductProperties(
            D: data["D"].toString().toLowerCase() == "true",
            V: data["V"].toString().toLowerCase() == "true",
            G: data["G"].toString().toLowerCase() == "true",
            N: data["N"].toString().toLowerCase() == "true",
            VE: data["VE"].toString().toLowerCase() == "true",
            offer: (data["offer"]??0)==1,
            category: data["categories"]??"",
            spiced: data["spiced"]??"",
            sortOrder: int.parse(data["sort_order"]??"0"),
            printOrder: int.parse(data["print_order"]??"0"),
            availableOn: data["available_on"]??"",
            offerPrice: double.parse(data["offer_price"]??"0.0"),
         );
   }
}

extension BoolParsing on String {
  bool parseBool() {
    return this.toLowerCase() == 'true';
  }
}
