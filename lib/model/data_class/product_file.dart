class ProductFile {
  String fileUid, keyName, itemId;

  ProductFile({
    required this.fileUid ,required this.keyName ,required this.itemId,
  });

  factory ProductFile.fromJSON(data){
    return ProductFile(
        fileUid: data["file_uuid"],
        keyName: data["key_name"],
        itemId: data["product_uuid"],
    );
  }
}