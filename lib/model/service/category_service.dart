import 'dart:convert';

import 'package:redmango_customer/model/data_class/category_model.dart';
import 'package:redmango_customer/model/data_class/response_model.dart';
import 'package:http/http.dart' as http;

import 'http_client.dart';

class CategoryService{
  Future<ApiResponse> getAllCategory() async{
    List<CategoryModel> allCategory = [];

    http.Response? response = await ApiClient.get("config/product/property/category");
    if(response == null){
      return ApiResponse(statusCode: 0,message: "Failed to connect !");
    }else{
      if(response.statusCode == 200){
        var data = jsonDecode(response.body);
        for(var d in data){
          CategoryModel categoryModel = CategoryModel.fromJSON(d);
          allCategory.add(categoryModel);
        }
        return ApiResponse(statusCode: 200,data: allCategory,);
      } else{
        return ApiResponse(statusCode: 500,message: "Unexpected error . please try again",data: allCategory);
      }
    }
  }
}