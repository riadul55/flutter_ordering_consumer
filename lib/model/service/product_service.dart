import 'dart:convert';

import 'package:redmango_customer/model/data_class/product_model.dart';
import 'package:redmango_customer/model/data_class/response_model.dart';
import 'package:http/http.dart' as http;

import 'http_client.dart';

class ProductService{
  Future<ApiResponse> getAllProduct(String categoryValue) async{
    List<ProductModel> categoryProducts = [];

    http.Response? response = await ApiClient.get("product?categories=$categoryValue&show_hierarchy=true");
    if(response == null){
      return ApiResponse(statusCode: 0,message: "Failed to connect !",data: categoryProducts);
    }else{
      if(response.statusCode == 200){
        var data = jsonDecode(response.body);
        for(var d in data){
          ProductModel model = ProductModel.fromJSON(d);
          categoryProducts.add(model);
        }
        return ApiResponse(statusCode: 200,data: categoryProducts,);
      } else{
        return ApiResponse(statusCode: 500,message: "Unexpected error . please try again",data: categoryProducts);
      }
    }
  }
}