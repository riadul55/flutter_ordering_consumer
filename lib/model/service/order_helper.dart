import 'package:redmango_customer/model/data_class/cart_item_model.dart';
import 'package:redmango_customer/model/data_class/component_section.dart';
import 'package:redmango_customer/model/data_class/product_model.dart';

bool alreadyExist(CartItemModel item,List<CartItemModel> cartItems){
  List<bool> anyExist = [];
  int j = 0;
  for(CartItemModel cartItem in cartItems){
    anyExist.add(false);
    if(cartItem.uuid == item.uuid){
      if(item.componentSections.isEmpty){
        anyExist[j]=true;
        break;
      }else if(item.componentSections.length!=cartItem.componentSections.length){
        anyExist[j] = false;
        break;
      }else{
        List<bool> data = [];
        for(int i = 0;i<cartItem.componentSections.length;i++){
          data.add(false);
          ComponentSection section1 = cartItem.componentSections[i];
          ComponentSection section2 = item.componentSections[i];
          if(section1.sectionValue == section2.sectionValue){
            if(section1.selectedItem!= null && section2.selectedItem != null){
              if(section1.selectedItem!.productUUid == section2.selectedItem!.productUUid){
                data[i] = true;
              }
            }
          }

        }
        anyExist[j] = true;
        for(bool b in data){
          if(!b){
            anyExist[j] = false;
            break;
          }
        }
      }
    }
    j++;
  }
  bool finalExist = false;
  for(bool b in anyExist){
    if(b){
      finalExist = true;
      break;
    }
  }
  return finalExist;
}


bool alreadyProductExist(ProductModel item,List<CartItemModel> cartItems){
  bool exist = false;
  for(CartItemModel cartItem in cartItems){
    if(cartItem.uuid == item.productUUid){
        exist = true;
    }

  }
  return exist;
}
