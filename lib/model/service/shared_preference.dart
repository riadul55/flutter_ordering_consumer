import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:redmango_customer/model/data_class/cart_item_model.dart';
import 'package:redmango_customer/model/data_class/product_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefManager{

  static Future<bool> alreadyExist(ProductModel product) async{
    List<CartItemModel> cartItems = [];
    bool alreadyExist = false;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> oldItems =  sharedPreferences.getStringList("cartItems")??[];
    for (var element in oldItems) {
      CartItemModel model = CartItemModel.fromJSON(jsonDecode(element));
      if(model.uuid==product.productUUid){
        alreadyExist = true;
      }
      cartItems.add(model);
    }
    return alreadyExist;
  }

  static  setCartItem(List<CartItemModel> newItems) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> strItems = [];
    for (CartItemModel element in newItems) {
      strItems.add(jsonEncode(CartItemModel.toJSON(itemModel: element)));
    }
    sharedPreferences.setStringList("cartItems", strItems);
  }


  static void addToCart(context,CartItemModel newItem) async{
    List<CartItemModel> cartItems = [];
    bool alreadyExist = false;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> oldItems =  sharedPreferences.getStringList("cartItems")??[];
    for (var element in oldItems) {
      CartItemModel model = CartItemModel.fromJSON(jsonDecode(element));
      if(model.uuid==newItem.uuid){
        alreadyExist = true;
      }
      cartItems.add(model);
    }
    if(alreadyExist){
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Item already in cart")));
    }else{
      oldItems.add(jsonEncode(CartItemModel.toJSON(itemModel: newItem)));
    }
    sharedPreferences.setStringList("cartItems", oldItems);
  }

  static void removeFromCart(CartItemModel newItem) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> oldItems =  sharedPreferences.getStringList("cartItems")??[];
    List<String> newItems =  [];
    for (var element in oldItems) {
      CartItemModel model = CartItemModel.fromJSON(jsonDecode(element));
      if(model.uuid!=newItem.uuid){
        newItems.add(jsonEncode(CartItemModel.toJSON(itemModel: newItem)));
      }
    }
    sharedPreferences.setStringList("cartItems", newItems);
  }

  static void increment(CartItemModel newItem) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> oldItems =  sharedPreferences.getStringList("cartItems")??[];
    List<String> newItems = [];
    for (var element in oldItems) {
      CartItemModel model = CartItemModel.fromJSON(jsonDecode(element));
      if(model.uuid==newItem.uuid){
        model.quantity++;
        model.subTotal =model.subTotal;
        model.total = model.quantity * model.subTotal;
      }
      newItems.add(jsonEncode(CartItemModel.toJSON(itemModel: newItem)));
    }

    sharedPreferences.setStringList("cartItems", newItems);
  }
  static void decrement(CartItemModel newItem) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> oldItems =  sharedPreferences.getStringList("cartItems")??[];
    List<String> newItems = [];
    for (var element in oldItems) {
      CartItemModel model = CartItemModel.fromJSON(jsonDecode(element));
      if(model.uuid==newItem.uuid){
        if(model.quantity > 1){
          model.quantity--;
        }
        model.subTotal =model.subTotal;
        model.total = model.quantity * model.subTotal;
      }
      newItems.add(jsonEncode(CartItemModel.toJSON(itemModel: newItem)));
    }

    sharedPreferences.setStringList("cartItems", newItems);
  }
  static Future<List<CartItemModel>> getCartItems() async{
    List<CartItemModel> cartItems = [];
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> oldItems =  sharedPreferences.getStringList("cartItems")??[];
    for (var element in oldItems) {
      CartItemModel model = CartItemModel.fromJSON(jsonDecode(element));
      cartItems.add(model);
    }
    return cartItems;
  }

  static void clearCart() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setStringList("cartItems", []);
  }

  // static Future<bool> alreadyExistFavoriteItems(ProductModel product) async{
  //   bool alreadyExist = false;
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   List<String> oldItems =  sharedPreferences.getStringList("FavoriteItems")??[];
  //   for (var element in oldItems) {
  //     ProductModel model = ProductModel.fromJSON(jsonDecode(element));
  //     if(model.productUUid==product.productUUid){
  //       alreadyExist = true;
  //     }
  //   }
  //   return alreadyExist;
  // }
  //
  // static void addToFavoriteItems(context,ProductModel newItem) async{
  //   List<ProductModel> favorites = [];
  //   bool alreadyExist = false;
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   List<String> oldItems =  sharedPreferences.getStringList("FavoriteItems")??[];
  //   for (var element in oldItems) {
  //     ProductModel model = ProductModel.fromJSON(jsonDecode(element));
  //     if(model.productUUid==newItem.productUUid){
  //       alreadyExist = true;
  //     }
  //     favorites.add(model);
  //   }
  //   if(alreadyExist){
  //     removeFromFavoriteItems(newItem);
  //   }else{
  //     oldItems.add(jsonEncode(ProductModel.toJSON(newItem)));
  //   }
  //   sharedPreferences.setStringList("FavoriteItems", oldItems);
  // }
  //
  // static void removeFromFavoriteItems(ProductModel newItem) async{
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   List<String> oldItems =  sharedPreferences.getStringList("FavoriteItems")??[];
  //   List<String> newItems =  [];
  //   for (var element in oldItems) {
  //     ProductModel model = ProductModel.fromJSON(jsonDecode(element));
  //     if(model.productUUid!=newItem.productUUid){
  //       newItems.add(jsonEncode(ProductModel.toJSON(model)));
  //     }
  //   }
  //   sharedPreferences.setStringList("FavoriteItems", newItems);
  // }
  //
  //
  //
  // static Future<List<ProductModel>> getFavoriteItems() async{
  //   List<ProductModel> favourites = [];
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   List<String> oldItems =  sharedPreferences.getStringList("FavoriteItems")??[];
  //   for (var element in oldItems) {
  //     ProductModel model = ProductModel.fromJSON(jsonDecode(element));
  //     favourites.add(model);
  //   }
  //   return favourites;
  // }
}