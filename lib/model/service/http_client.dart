import 'package:http/http.dart' as http;
class ApiClient{
  static const baseURL = "https://labapi.yuma-technology.co.uk:8443/delivery/";

  static Future<http.Response?> get(urlPath) async {
    try {
      Uri url = Uri.parse(baseURL + urlPath);
      http.Response response = await http.get(
          url,
          headers: {
            'content-type':'application/json',
          }
      ).timeout(const Duration(seconds: 60));
        return response;
    } catch (e) {
      return null;
    }
  }

}